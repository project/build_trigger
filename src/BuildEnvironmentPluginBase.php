<?php

namespace Drupal\build_trigger;

use Drupal\build_trigger\Entity\BuildJob;
use Drupal\build_trigger\Entity\BuildJobInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

;

/**
 * Base class for build_environment plugins.
 */
abstract class BuildEnvironmentPluginBase extends PluginBase implements BuildEnvironmentPluginInterface, ContainerFactoryPluginInterface, PluginWithFormsInterface {

  use PluginWithFormsTrait;
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface;
   */
  protected ClientInterface $httpClient;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->logger = $logger_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * Trigger the build.
   *
   * @param \Drupal\build_trigger\Entity\BuildJobInterface $build_job
   *   The build job to trigger.
   *
   * @returns \GuzzleHttp\Psr7\Response
   *   Returns the response.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  abstract protected function triggerBuild(BuildJobInterface $build_job): Response;

  /**
   * Get latest build status.
   *
   * @param \Drupal\build_trigger\Entity\BuildJobInterface $build_job
   *    The build job to trigger.
   *
   * @returns string|null
   *   Returns the latest build status or null if not supported.
   * .
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  abstract protected function updateBuild(BuildJobInterface $build_job): string|NULL;

  /**
   * {@inheritdoc}
   */
  public function build(BuildJob $build_job): string {
    try {
      $response = $this->triggerBuild($build_job);
      $details = $build_job->getDetails();

      if ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) {
        $details['message'] = $response->getReasonPhrase();
        $build_job
          ->setStatus(BuildJobInterface::STATUS_CREATED)
          ->setDetails($details)
          ->save();
        $this->messenger->addStatus($this->t('Build triggered for @env environment.', [
          '@env' => $build_job->getEnvironment()->label(),
        ]));
        return BuildJobInterface::STATUS_CREATED;
      }
      else {
        $details['message'] = $response->getReasonPhrase();
        $build_job
          ->setDetails($details)
          ->setStatus(BuildJobInterface::STATUS_ERROR)
          ->save();
        $this->messenger->addError($this->t('There was a problem building the @env environment.', [
          '@env' => $build_job->getEnvironment()->label(),
        ]));
        $this->logger->get('build_trigger')
          ->error('There was a problem building the @env environment. Message @message', [
            '@env' => $build_job->getEnvironment()->label(),
            '@message' => $response->getReasonPhrase(),
          ]);
        return BuildJobInterface::STATUS_ERROR;
      }
    }
    catch (GuzzleException $exception) {
      $details['message'] = $exception->getMessage();
      $build_job
        ->setDetails($details)
        ->setStatus(BuildJobInterface::STATUS_ERROR)
        ->save();
      $this->messenger->addError($this->t('There was a problem building the @env environment.', [
        '@env' => $build_job->getEnvironment()->label(),
      ]));
      $this->logger->get('build_trigger')
        ->error('There was a problem building the @env environment. Error message @message', [
          '@env' => $build_job->getEnvironment()->label(),
          '@message' => $exception->getMessage(),
        ]);
      return BuildJobInterface::STATUS_ERROR;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $details): array {
    return [
      '#markup' => isset($details['message']) ? '<p>' . $details['message'] . '</p>' : '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function update(BuildJob $build_job): string {
    try {
      $status = $this->updateBuild($build_job);
      if (!is_null($status)) {
        $build_job->setStatus($status);
        $build_job->save();

        return $status;
      }
    }
    catch (GuzzleException $exception) {
      $details = $build_job->getDetails();
      $details['message'] = $exception->getMessage();
      $build_job
        ->setDetails($details)
        ->setStatus(BuildJobInterface::STATUS_ERROR)
        ->save();
      $this->messenger->addError($this->t('There was a problem fetching updates for the @env environment.', [
        '@env' => $build_job->getEnvironment()->label(),
      ]));
      $this->logger->get('build_trigger')
        ->error('There was a problem fetching updates for the @env environment. Error message @message', [
          '@env' => $build_job->getEnvironment()->label(),
          '@message' => $exception->getMessage(),
        ]);
    }

    return BuildJobInterface::STATUS_UNKNOWN;
  }


  /**
   * {@inheritdoc}
   */
  public function buildBuildForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateBuildForm(array &$form, FormStateInterface $form_state): void {
    // Validation is optional.
  }

  /**
   * {@inheritdoc}
   */
  public function submitBuildForm(array &$form, FormStateInterface $form_state): void {
    // Submission is optional.
  }

  /**
   * Returns generic default configuration for build environments.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseConfigurationDefaults(): array {
    return [
      'id' => $this->getPluginId(),
      'provider' => $this->pluginDefinition['provider'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = [];
    $definition = $this->getPluginDefinition();
    $dependencies['module'][] = $definition['provider'];
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->baseConfigurationDefaults(),
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $definition = $this->getPluginDefinition();
    $form['provider'] = [
      '#type' => 'value',
      '#value' => $definition['provider'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    if (!$form_state->getErrors()) {
      $this->configuration['provider'] = $form_state->getValue('provider');
    }
  }

}
