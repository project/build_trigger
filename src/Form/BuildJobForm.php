<?php

namespace Drupal\build_trigger\Form;

use Drupal\build_trigger\Entity\BuildJobInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the build entity edit forms.
 */
class BuildJobForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Set defaults.
    /** @var \Drupal\build_trigger\Entity\BuildJob $build_job */
    $build_job = $form_state->getFormObject()->getEntity();
    $build_job->setStatus(BuildJobInterface::STATUS_UNKNOWN);
    $build_job->setOwnerId($build_job::getDefaultEntityOwner());

    // Add plugin build-time form.
    $build_environment_plugin = $build_job->getEnvironment()->getPlugin();
    $form = $build_environment_plugin->buildBuildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    // Validate plugin build-time form.
    /** @var \Drupal\build_trigger\Entity\BuildJob $build_job */
    $build_job = $form_state->getFormObject()->getEntity();
    $build_environment_plugin = $build_job->getEnvironment()->getPlugin();
    $build_environment_plugin->validateBuildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    // Submit plugin build-time form.
    /** @var \Drupal\build_trigger\Entity\BuildJob $build_job */
    $build_job = $form_state->getFormObject()->getEntity();
    $build_environment_plugin = $build_job->getEnvironment()->getPlugin();
    $build_environment_plugin->submitBuildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $args = ['%label' => $this->entity->id()];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New build %label has been created.', $args));
        $this->logger('build_trigger')->notice('New build %label has been created.', $args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The build %label has been updated.', $args));
        $this->logger('build_trigger')->notice('The build %label has been updated.', $args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

}
