<?php

namespace Drupal\build_trigger\Form;

use Drupal\build_trigger\BuildEnvironmentPluginInterface;
use Drupal\build_trigger\Entity\BuildEnvironment;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Build environment form.
 */
class BuildEnvironmentForm extends EntityForm {

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_manager
   *   The plugin form manager.
   */
  public function __construct(PluginFormFactoryInterface $plugin_form_manager) {
    $this->pluginFormFactory = $plugin_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin_form.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\build_trigger\Entity\BuildEnvironment $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [BuildEnvironment::class, 'load'],
      ],
      '#disabled' => !$entity->isNew(),
    ];
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Url'),
      '#maxlength' => 255,
      '#default_value' => $entity->getUrl(),
      '#description' => $this->t('Url at which this environment is available for viewing.'),
      '#required' => TRUE,
    ];
    $form['colour'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Colour'),
      '#maxlength' => 8,
      '#default_value' => $entity->getColour(),
      '#description' => $this->t('A colour for this environment in hex form.'),
    ];
    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#max' => 100,
      '#min' => -100,
      '#size' => 3,
      '#default_value' => $entity->getWeight() ? $entity->getWeight() : 0,
      '#description' => $this->t('Set the weight, lighter environments will be rendered first in listings.'),
      '#required' => TRUE,
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity->status(),
    ];
    $form['#tree'] = TRUE;
    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings'] = $this->getPluginForm($entity->getPlugin())
      ->buildConfigurationForm($form['settings'], $subform_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $sub_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $plugin = $this->entity->getPlugin();
    $this->getPluginForm($plugin)
      ->submitConfigurationForm($form, $sub_form_state);
    $this->entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new environment %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated environment %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginForm(BuildEnvironmentPluginInterface $buildEnvironment) {
    return $this->pluginFormFactory->createInstance($buildEnvironment, 'configure');
  }

}
