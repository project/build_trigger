<?php

namespace Drupal\build_trigger\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class BuildHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $route_collection = new RouteCollection();
    $route = (new Route('/admin/builds/{build_job}'))
      ->addDefaults([
        '_controller' => '\Drupal\build_trigger\Controller\BuildJobViewController::view',
        '_title_callback' => '\Drupal\build_trigger\Controller\BuildJobViewController::title',
      ])
      ->setRequirement('build_job', '\d+')
      ->setRequirement('_permission', 'build trigger build');
    $route_collection->add('entity.build_job.canonical', $route);

    $route = (new Route('/admin/builds/{build_job}/delete'))
      ->addDefaults([
        '_entity_form' => 'build_job.delete',
        '_title' => 'Delete',
      ])
      ->setRequirement('build_job', '\d+')
      ->setRequirement('_permission', 'build trigger build');
    $route_collection->add('entity.build_job.delete_form', $route);

    return $route_collection;
  }
}
