<?php

namespace Drupal\build_trigger\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines build_environment annotation object.
 *
 * @Annotation
 */
class BuildEnvironment extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

}
