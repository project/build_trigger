<?php

namespace Drupal\build_trigger;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\TypedData\Plugin\DataType\Uri;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the build entity type.
 */
class BuildJobListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a new NodeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['name'] = $this->t('Job');
    $header['status'] = $this->t('Status');
    $header['complete'] = $this->t('Completed');
    $header['environment'] = $this->t('Environment');
    $header['uid'] = $this->t('Triggered by');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\build_trigger\Entity\BuildJobInterface $entity */

    $row['name']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->toUrl(),
    ];
    $row['status']['data'] = [
      '#prefix' => '<span id="build-job-status-' . $entity->id() . '">',
      '#markup' => $entity->getStatus(),
      '#suffix' => '</span>',
    ];
    $row['complete']['data'] = [
      '#prefix' => '<span id="build-job-complete-' . $entity->id() . '">',
      '#markup' => $entity->isActive() ? 'No' : 'Yes',
      '#suffix' => '</span>',
    ];
    $row['environment']['data'] = [
      '#type' => 'link',
      '#title' => $entity->getEnvironment()->label(),
      '#url' => Url::fromUri($entity->getEnvironment()->getUrl()),
    ];
    $row['author']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['created']['data'] = $this->dateFormatter->format($entity->getCreatedTime(), 'custom', 'Y-m-d H:i:s');
    $row['changed']['data'] = [
      '#prefix' => '<span id="build-job-changed-' . $entity->id() . '">',
      '#markup' => $this->dateFormatter->format($entity->getChangedTime(), 'custom', 'Y-m-d H:i:s'),
      '#suffix' => '</span>',
    ];
    $row['operations']['data'] = $this->buildOperations($entity);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 10,
        'url' => $entity->toUrl('canonical'),
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityListQuery(): QueryInterface {
    $query = parent::getEntityListQuery();
    $query->sort('id', 'DESC');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();
    $build['#attached']['library'][] = 'build_trigger/build_jobs';
    return $build;
  }

}
