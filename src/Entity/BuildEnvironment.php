<?php

namespace Drupal\build_trigger\Entity;

use Drupal\build_trigger\BuildEnvironmentPluginCollection;
use Drupal\build_trigger\BuildEnvironmentPluginInterface;
use Drupal\Component\Plugin\LazyPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the build environment entity type.
 *
 * @ConfigEntityType(
 *   id = "build_environment",
 *   label = @Translation("Build environment"),
 *   label_collection = @Translation("Build environments"),
 *   label_singular = @Translation("build environment"),
 *   label_plural = @Translation("build environments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count build environment",
 *     plural = "@count build environments",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\build_trigger\BuildEnvironmentListBuilder",
 *     "form" = {
 *       "default" = "Drupal\build_trigger\Form\BuildEnvironmentForm",
 *       "edit" = "Drupal\build_trigger\Form\BuildEnvironmentForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "build_environment",
 *   admin_permission = "administer build_environment",
 *   links = {
 *     "collection" = "/admin/config/build-trigger/environments",
 *     "add-form" = "/admin/config/build-trigger/environments/plugins",
 *     "edit-form" =
 *   "/admin/config/build-trigger/environments/{build_environment}",
 *     "delete-form" =
 *   "/admin/config/build-trigger/environments/{build_environment}/delete",
 *     "collection" = "/admin/config/build-trigger/environments/",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "colour",
 *     "weight",
 *     "plugin",
 *     "settings",
 *     "status",
 *     "url",
 *   },
 * )
 */
class BuildEnvironment extends ConfigEntityBase implements BuildEnvironmentInterface, EntityWithPluginCollectionInterface {

  /**
   * The build environment ID.
   */
  protected string $id;

  /**
   * The build environment label.
   */
  protected string $label;

  /**
   * The build environment colour.
   */
  protected string $colour = '';

  /**
   * The plugin instance ID.
   *
   * @var string
   */
  protected string $plugin;

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\build_trigger\BuildEnvironmentPluginCollection|null
   */
  protected BuildEnvironmentPluginCollection|NULL $pluginCollection = NULL;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected array $settings = [];

  /**
   * The url of the environment.
   *
   * @var string
   */
  protected string $url = '';

  /**
   * The weight of the environment.
   *
   * @var int
   */
  protected int $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function build(BuildJob $build_job) {
    $this->getPlugin()->build($build_job);
  }

  /**
   * {@inheritdoc}
   */
  public function getColour(): string {
    return $this->colour;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): string {
    return $this->url;
  }

  public function getWeight(): int  {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): BuildEnvironmentPluginInterface {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections(): array {
    return [
      'settings' => $this->getPluginCollection(),
    ];
  }

  /**
   * Encapsulates creation of the frontend environment's LazyPluginCollection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The build environment's plugin collection.
   */
  protected function getPluginCollection(): LazyPluginCollection {
    if (is_null($this->pluginCollection)) {
      $this->pluginCollection = new BuildEnvironmentPluginCollection(\Drupal::service('plugin.manager.build_environment'), $this->plugin, $this->get('settings'));
    }
    return $this->pluginCollection;
  }

}
