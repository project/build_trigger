<?php

namespace Drupal\build_trigger\Entity;

use Drupal\build_trigger\BuildEnvironmentPluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a build environment entity type.
 */
interface BuildEnvironmentInterface extends ConfigEntityInterface {

  /**
   * Trigger build job.
   *
   * @param \Drupal\build_trigger\Entity\BuildJob $build_job
   *   The build job to update for the build.
   */
  public function build(BuildJob $build_job);

  /**
   * Get the colour for the environment.
   *
   * @return string
   *   The colour.
   */
  public function getColour(): string;

  /**
   * Gets the build environment plugin for this environment.
   *
   * @return \Drupal\build_trigger\BuildEnvironmentPluginInterface
   *   The build environment plugin.
   */
  public function getPlugin(): BuildEnvironmentPluginInterface;

  /**
   * Get the URL of the environment.
   *
   * @return string
   *   The environment URL.
   */
  public function getUrl(): string;

  /**
   * Get the weight of the environment.
   *
   * @return int
   *   The environment weight.
   */
  public function getWeight(): int;

}
