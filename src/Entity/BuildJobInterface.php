<?php

namespace Drupal\build_trigger\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a build entity type.
 */
interface BuildJobInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  const STATUS_CREATED = 'created';
  const STATUS_PENDING = 'pending';
  const STATUS_RUNNING = 'running';
  const STATUS_SUCCESS = 'success';
  const STATUS_FAILED = 'failed';
  const STATUS_SKIPPED = 'skipped';
  const STATUS_ERROR = 'error';
  const STATUS_UNKNOWN = 'unknown';

  /**
   * Is the job currently active.
   *
   * @return bool
   */
  public function isActive(): bool;

  /**
   * Get the created timestamp.
   *
   * @return int
   */
  public function getCreatedTime(): int;

  /**
   * Get the plugin details.
   *
   * @return array|null
   */
  public function getDetails(): ?array;

  /**
   * Set any details returned by the plugin.
   *
   * @param mixed $details
   *   Plugin details. Must be serializable as JSON.
   *
   * @return static
   */
  public function setDetails(mixed $details): static;

  /**
   * Get the environment.
   *
   * @return \Drupal\build_trigger\Entity\BuildEnvironment
   */
  public function getEnvironment(): BuildEnvironment;

  /**
   * Get the environment ID.
   *
   * @return string
   */
  public function getEnvironmentId(): string;

  /**
   * Get the build status code.
   *
   * @return string
   */
  public function getStatus(): string;

  /**
   * Set the build status code.
   *
   * @param string $status_code
   *   The status code.
   *
   * @return static
   *
   * @throws \InvalidArgumentException
   */
  public function setStatus(string $status_code): static;
}
