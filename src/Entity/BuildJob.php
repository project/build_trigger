<?php

namespace Drupal\build_trigger\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the build entity class.
 *
 * @ContentEntityType(
 *   id = "build_job",
 *   label = @Translation("Build job"),
 *   label_collection = @Translation("Builds"),
 *   label_singular = @Translation("build job"),
 *   label_plural = @Translation("build jobs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count build job",
 *     plural = "@count build jobs",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\build_trigger\BuildJobListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\build_trigger\Form\BuildJobForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" =
 *   "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\build_trigger\Routing\BuildHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "build_job",
 *   admin_permission = "administer build jobs",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "active" = "active",
 *     "environment_id" = "environment_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/builds",
 *     "canonical" = "/admin/builds/{build_job}",
 *     "delete-form" = "/admin/builds/{build_job}/delete",
 *     "delete-multiple-form" = "/admin/builds/delete-multiple",
 *   },
 * )
 */
class BuildJob extends ContentEntityBase implements BuildJobInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * The build environment.
   *
   * @var string
   */
  protected string $environment;

  /**
   * {@inheritdoc}
   */
  public function label(): string|TranslatableMarkup|NULL {
    return '#' . $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDetails(): array {
    $json = $this->get('details')->value;
    if (empty($json)) {
      return [];
    }
    return json_decode($json, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function setDetails(mixed $details): static {
    $json = json_encode($details);
    $this->set('details', $json);
    return $this;
  }

  /**
   * {@inheritdoc }
   */
  public function getEnvironment(): BuildEnvironment {
    return $this->get('environment_id')->entity;
  }

  /**
   * {@inheritdoc }
   */
  public function getEnvironmentId(): string {
    return $this->get('environment_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status_code): static {
    if (!$this->validStatus($status_code)) {
      throw new \InvalidArgumentException('Invalid status code ' . $status_code);
    }

    $this->set('status', $status_code);
    $this->set('active', $this->isActive());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool {
    return in_array($this->getStatus(), $this->getActiveStatuses());
  }

  /**
   * Get active status codes.
   *
   * @retun string[]
   */
  protected function getActiveStatuses(): array {
    return [
      BuildJobInterface::STATUS_CREATED,
      BuildJobInterface::STATUS_PENDING,
      BuildJobInterface::STATUS_RUNNING,
    ];
  }

  /**
   * Validate a status code.
   *
   * @param $status_code string
   *   Status code to validate.
   *
   * @returns boolean
   *   Returns true if the status code is valid, otherwise false.
   */
  protected function validStatus(string $status_code): bool {
    switch ($status_code) {
      case BuildJobInterface::STATUS_CREATED:
      case BuildJobInterface::STATUS_PENDING:
      case BuildJobInterface::STATUS_RUNNING:
      case BuildJobInterface::STATUS_SUCCESS:
      case BuildJobInterface::STATUS_FAILED:
      case BuildJobInterface::STATUS_SKIPPED:
      case BuildJobInterface::STATUS_ERROR:
      case BuildJobInterface::STATUS_UNKNOWN:
        return TRUE;
      default:
        return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {

    // Trigger build when the BuildJob is created.
    if (!$update) {
      $this->getEnvironment()->build($this);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['environment_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Build environment'))
      ->setSetting('target_type', 'build_environment')
      ->setRequired(true);

    $fields['message'] = BaseFieldDefinition::create('string')
      ->setLabel('Build message');

    $fields['details'] = BaseFieldDefinition::create('string_long')
      ->setLabel('Plugin details');

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel('Build status')
      ->setSettings([
        'default_value' => BuildJobInterface::STATUS_UNKNOWN,
        'max_length' => 12,
      ]);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Active job');

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Started by'))
      ->setSetting('target_type', 'user');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Built on'))
      ->setDescription(t('The time that the build was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last updated'))
      ->setDescription(t('The time that the build was last updated.'));

    return $fields;
  }

}
