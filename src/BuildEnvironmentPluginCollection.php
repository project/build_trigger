<?php

namespace Drupal\build_trigger;

use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Provides a collection of build environment plugins.
 */
class BuildEnvironmentPluginCollection extends DefaultSingleLazyPluginCollection {

}
