<?php

namespace Drupal\build_trigger;

use Drupal\build_trigger\Annotation\BuildEnvironment;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * BuildEnvironment plugin manager.
 */
class BuildEnvironmentPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BuildEnvironment', $namespaces, $module_handler, BuildEnvironmentPluginInterface::class, BuildEnvironment::class);
    $this->alterInfo('build_environment_info');
    $this->setCacheBackend($cache_backend, 'build_environment_plugins');
  }

}
