<?php

namespace Drupal\build_trigger;

use Drupal\build_trigger\Entity\BuildJob;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for build_environment plugins.
 */
interface BuildEnvironmentPluginInterface extends ConfigurableInterface, DependentPluginInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Trigger build job.
   *
   * @param \Drupal\build_trigger\Entity\BuildJob $build_job
   *   The build job to update for the build.
   *
   * @return string
   *   Returns the build job status.
   */
  public function build(BuildJob $build_job): string;

  /**
   * Fetch update for build job.
   *
   * @param \Drupal\build_trigger\Entity\BuildJob $build_job
   *   The build job to update for the build.
   *
   * @return string
   *   Returns the build job status.
   */
  public function update(BuildJob $build_job): string;

  /**
   * Render details saved in the build job.
   *
   * @param array $details
   *   Details saved in the build job.
   *
   * @return array
   *   Render array for details.
   */
  public function render(array $details): array;

  /**
   * Get the build-time configuration form.
   *
   * Form used to gather configuration needed to execute build.
   *
   * @param array $form
   *    An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *    The current state of the form.
   *
   * @return array
   *   The configuration form structure.
   */
  public function buildBuildForm(array $form, FormStateInterface $form_state): array;

  /**
   * Build-time form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   *  @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateBuildForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Build-time form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitBuildForm(array &$form, FormStateInterface $form_state): void;
}
