<?php

namespace Drupal\build_trigger\Plugin\BuildEnvironment;

use Drupal\build_trigger\BuildEnvironmentPluginBase;
use Drupal\build_trigger\Entity\BuildJobInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Psr7\Response;

/**
 * Simple build hook plugin.
 *
 * @BuildEnvironment(
 *   id = "build_hook",
 *   label = @Translation("Build hook"),
 *   description = @Translation("Simple hook to trigger environment build.")
 * )
 */
class BuildHook extends BuildEnvironmentPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function triggerBuild(BuildJobInterface $build_job): Response {

    // Trigger build hook,
    $response = $this->httpClient->request(
      $this->configuration['build_hook_method'] ?? 'POST',
      $this->configuration['build_hook_url'],
    );

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function updateBuild(BuildJobInterface $build_job): string|NULL {

    // We aren't able to check the build status with a basic build hook.
    // If build hook was triggered successfully return success.
    if ($build_job->getStatus() == BuildJobInterface::STATUS_CREATED) {
      return BuildJobInterface::STATUS_SUCCESS;
    }

    return $build_job->getStatus();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    // For this type of plugin, we only need the build hook method and URL.
    $form['build_hook_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Build hook URL'),
      '#maxlength' => 255,
      '#default_value' => $this->configuration['build_hook_url'] ?? '',
      '#description' => $this->t('Build hook URL for the environment.'),
      '#required' => TRUE,
    ];
    $form['build_hook_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Build hook HTTP method'),
      '#description' => $this->t('HTTP method required by the build hook.'),
      '#options' => [
        'DELETE' => 'DELETE',
        'GET' => 'GET',
        'PATCH' => 'PATCH',
        'POST' => 'POST',
        'PUT' => 'PUT',
      ],
      '#default_value' => $this->configuration['build_hook_method'] ?? 'POST',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'build_hook_method' => 'POST',
      'build_hook_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $this->configuration['build_hook_method'] = $form_state->getValue('build_hook_method');
      $this->configuration['build_hook_url'] = $form_state->getValue('build_hook_url');
    }
  }

}
