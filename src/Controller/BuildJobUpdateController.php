<?php

namespace Drupal\build_trigger\Controller;

use Drupal\build_trigger\Entity\BuildJob;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Build trigger routes.
 */
class BuildJobUpdateController extends ControllerBase {

  /**
   * Builds the API response.
   *
   * @param \Drupal\build_trigger\Entity\BuildJob $build_job
   *   Build job to update.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON API response.
   */
  public function api(BuildJob $build_job): JsonResponse {

    // Update job.
    $plugin = $build_job->getEnvironment()->getPlugin();
    $status = $plugin->update($build_job);

    // Reload job and return JSON response.
    $build_job = $this->entityTypeManager()
      ->getStorage('build_job')
      ->load($build_job->id());
    $data = [
      'id' => $build_job->id(),
      'status' => $status,
      'environment' => $build_job->getEnvironment()->id(),
      'settings' => $build_job->getSettings(),
      'uid' => $build_job->getOwnerId(),
      'changed' => $build_job->getChangedTime(),
    ];

    return new JsonResponse($data);
  }

  /**
   * Update all active builds.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect back to build job listing.
   */
  public function all(): RedirectResponse {

    $active_jobs = $this->entityTypeManager()
      ->getStorage('build_job')
      ->loadByProperties(['active' => 1]);
    foreach ($active_jobs as $job) {
      $plugin = $job->getEnvironment()->getPlugin();
      $plugin->update($job);
    }

    return $this->redirect('entity.build_job.collection');
  }

  /**
   * Update a single build.
   *
   * @param \Drupal\build_trigger\Entity\BuildJob $build_job
   *    Build job to update.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect back to build job listing.
   */
  public function one(BuildJob $build_job): RedirectResponse {

    $plugin = $build_job->getEnvironment()->getPlugin();
    $plugin->update($build_job);

    return $this->redirect('entity.build_job.canonical', ['build_job' => $build_job->id()]);
  }

}
