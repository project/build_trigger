<?php

namespace Drupal\build_trigger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Build trigger routes.
 */
class BuildTriggerController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $storage = $this->entityTypeManager()->getStorage('build_environment');
    $query = $storage->getQuery();
    $environment_ids = $query->sort('weight', 'ASC')->execute();
    $environments = $storage->loadMultiple($environment_ids);

    if (empty($environments)) {
      return [
        '#type' => 'inline_template',
        '#template' => '<p>{{ message }}</p>',
        '#context' => [
          'message' => 'No build environments defined.'
        ],
      ];
    }

    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    /** @var \Drupal\build_trigger\Entity\BuildEnvironment $environment */
    foreach ($environments as $environment) {
      $build['#bundles'][$environment->id()] = [
        'add_link' => Link::createFromRoute($environment->label(), 'build_trigger.build_job_new', [
          'build_environment' => $environment->id(),
        ]),
        'description' => 'Build ' . $environment->label() . ' environment (' . $environment->getUrl() . ')',
      ];
    }

    return $build;
  }

}
