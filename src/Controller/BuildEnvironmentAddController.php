<?php

namespace Drupal\build_trigger\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for building build environment instance form.
 */
class BuildEnvironmentAddController extends ControllerBase {

  /**
   * Add the Frontend environment form.
   *
   * @param string $plugin_id
   *   The plugin id of the frontend environment.
   *
   * @return array
   *   The form to add and configure a frontend environment entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __invoke($plugin_id): array {

    // Create a build environment entity.
    $entity = $this->entityTypeManager()->getStorage('build_environment')->create(['plugin' => $plugin_id]);
    return $this->entityFormBuilder()->getForm($entity);
  }

}
