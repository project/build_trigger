<?php

namespace Drupal\build_trigger\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render a build job.
 */
class BuildJobViewController extends EntityViewController {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $build_job, $view_mode = 'full', $langcode = NULL): array {
    return parent::view($build_job, $view_mode);
  }

  /**
   * The _title_callback for the page that renders a build job.
   *
   * @param \Drupal\Core\Entity\EntityInterface $build_job
   *   The current build job.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function title(EntityInterface $build_job): string|TranslatableMarkup {
    return $this->t('Build Job @label', ['@label' => $build_job->label()]);
  }

}
