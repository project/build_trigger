<?php

namespace Drupal\build_trigger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crete new Build entity.
 */
class BuildJobNewController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new GroupNodeController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Builds the response.
   *
   * @param string $build_environment
   *   Build environment ID.
   */
  public function __invoke(string $build_environment): array {

    // Create a build environment entity.
    /** @var \Drupal\build_trigger\Entity\BuildJob $entity */
    $entity = $this->entityTypeManager()->getStorage('build_job')
      ->create([
        'environment_id' => $build_environment,
      ]);

    $form = $this->entityFormBuilder()->getForm($entity, 'add');
    $form['#prefix'] = new TranslatableMarkup('<p>Trigger new build for the <strong>@environment</strong> environment.</p>', ['@environment' => $entity->getEnvironment()->label()]);
    $form['actions']['submit']['#value'] = $this->t('Build');

    return $form;
  }

}
