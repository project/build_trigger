<?php

namespace Drupal\build_trigger\Controller;

use Drupal\build_trigger\BuildEnvironmentPluginManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Build trigger routes.
 */
class BuildEnvironmentPluginsController extends ControllerBase {

  /**
   * The build environment plugin manager.
   *
   * @var \Drupal\build_trigger\BuildEnvironmentPluginManager
   */
  protected $buildEnvironmentPluginManager;

  /**
   * The controller constructor.
   */
  public function __construct(BuildEnvironmentPluginManager $plugin_manager) {
    $this->buildEnvironmentPluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('plugin.manager.build_environment'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $headers = [
      ['data' => $this->t('Type')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Operations')],
    ];

    $definitions = $this->buildEnvironmentPluginManager->getDefinitions();

    $rows = [];
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $row = [];
      $row['title'] = $plugin_definition['label'];
      $row['description']['data'] = $plugin_definition['description'];

      $links['add'] = [
        'title' => $this->t('Add new environment'),
        'url' => Url::fromRoute('build_trigger.build_environment_add', ['plugin_id' => $plugin_id]),
      ];

      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
      $rows[] = $row;
    }

    $build['build_environments'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No types available. Please enable one of the submodules or add your own custom plugin.'),
    ];

    return $build;
  }

}
