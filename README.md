# Build Trigger module

_The Build Trigger module is designed to trigger jobs that build and deploy the frontend
of a decoupled Drupal site.

The module provides a UI to configure environments to deploy and then to trigger a build
of the environment and check its status. Where supported, it's also possible to view the
log for a job.

This module doesn't handle the build itself, it is simply a frontend in Drupal to an
existing build system.

It currently supports a triggering a simple build hook and triggering a GitLab Ci
pipeline.

This module is in active development with a plan to add more build environments._
