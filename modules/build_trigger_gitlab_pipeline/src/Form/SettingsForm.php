<?php

namespace Drupal\build_trigger_gitlab_pipeline\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Gitlab pipeline plugin form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'build_trigger_gitlab_pipeline.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'build_trigger_gitlab_pipeline.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form =  parent::buildForm($form, $form_state);
    $config = $this->config('build_trigger_gitlab_pipeline.settings');

    $form['gitlab_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Gitlab URL'),
      '#description' => $this->t('URL of the Gitlab instance.'),
      '#required' => TRUE,
      '#default_value' => $config->get('gitlab_url') ?? '',
    ];
    $form['gitlab_api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gitlab API token'),
      '#description' => $this->t('API token with permissions to trigger the Gitlab pipeline and read its status.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('gitlab_api_token') ?? '',
    ];
    $form['gitlab_stages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pipeline stages'),
      '#description' => $this->t('Stages to display for the Gitlab pipeline.'),
      '#default_value' => $config->get('gitlab_stages') ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('build_trigger_gitlab_pipeline.settings');

    $config->set('gitlab_url', (string) $form_state->getValue('gitlab_url'))
      ->set('gitlab_api_token', (string) $form_state->getValue('gitlab_api_token'))
      ->set('gitlab_stages', (string) $form_state->getValue('gitlab_stages'))
      ->save(TRUE);

    parent::submitForm($form, $form_state);
  }

}
