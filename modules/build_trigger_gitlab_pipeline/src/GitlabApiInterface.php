<?php

namespace Drupal\build_trigger_gitlab_pipeline;

use Psr\Http\Message\ResponseInterface;

/**
 * Gitlab API request interface.
 */
interface GitlabApiInterface {

  /**
   * Create a new pipeline.
   *
   * @see https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline
   *
   * @param string $project_id
   *   Gitlab project id.
   * @param string $ref
   *   The branch or tag to run the pipeline on.
   * @param array $vars
   *   Variable for pass to Gitlab pipeline.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response from Gitlab.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createPipeline(string $project_id, string $ref, array $vars): ResponseInterface;

  /**
   * Get a pipeline.
   *
   * @see https://docs.gitlab.com/ee/api/pipelines.html#get-a-single-pipeline
   *
   * @param string $project_id
   *   Gitlab project id.
   * @param int $pipeline_id
   *   Gitlab pipeline id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response from Gitlab.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPipeline(string $project_id, int $pipeline_id): ResponseInterface;

  /**
   * List pipeline jobs.
   *
   * @see https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
   *
   * @param string $project_id
   *   Gitlab project id.
   * @param int $pipeline_id
   *   Gitlab pipeline id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response from Gitlab.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPipelineJobs(string $project_id, int $pipeline_id): ResponseInterface;

  /**
   * Get a a job.
   *
   * @see https://docs.gitlab.com/ee/api/jobs.html#get-a-single-job
   *
   * @param string $project_id
   *   Gitlab project id.
   * @param int $job_id
   *   Gitlab job id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *    Response from Gitlab.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getJob(string $project_id, int $job_id): ResponseInterface;

  /**
   * Get a log file for a job.
   *
   * @see https://docs.gitlab.com/ee/api/jobs.html#get-a-log-file
   *
   * @param string $project_id
   *   Gitlab project id.
   * @param int $job_id
   *   Gitlab job id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *    Response from Gitlab.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getJobLog(string $project_id, int $job_id): ResponseInterface;
}
