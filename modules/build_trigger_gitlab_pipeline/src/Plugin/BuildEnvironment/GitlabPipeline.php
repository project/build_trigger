<?php

namespace Drupal\build_trigger_gitlab_pipeline\Plugin\BuildEnvironment;

use Drupal\build_trigger\BuildEnvironmentPluginBase;
use Drupal\build_trigger\Entity\BuildJobInterface;
use Drupal\build_trigger_gitlab_pipeline\GitlabApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Gitlab pipeline plugin.
 *
 * @BuildEnvironment(
 *   id = "gitlab_pipeline",
 *   label = @Translation("Gitlab pipeline"),
 *   description = @Translation("Trigger pipeline using the Gitlab API.")
 * )
 */
class GitlabPipeline extends BuildEnvironmentPluginBase {

  /**
   * The Gitlab API service.
   *
   * @var \Drupal\build_trigger_gitlab_pipeline\GitlabApi
   */
  protected GitlabApi $gitlabApi;

  /**
   * The Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger, GitlabApi $gitlab_api, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory, $http_client, $logger_factory, $messenger);
    $this->gitlabApi = $gitlab_api;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('build_trigger_gitlab_pipeline.gitlab_api'),
      $container->get('request_stack'),
    );
  }

  /**
   * @inheritdoc
   */
  protected function triggerBuild(BuildJobInterface $build_job): Response {
    $project_id = $this->configuration['gitlab_project_id'];
    $ref = $this->configuration['gitlab_ref'];
    $vars = $this->configuration['gitlab_vars'];

    $variables = [];
    foreach (explode("\n", $vars) as $var) {
      $v = explode(':', $var);
      if (count($v) == 2) {
        $variables[trim($v[0])] = trim($v[1]);
      }
    }

    // Trigger pipeline.
    $response = $this->gitlabApi->createPipeline($project_id, $ref, $variables);
    $body = json_decode($response->getBody());
    $details = [
      'pipeline_id' => $body->id,
      'ref' => $body->ref,
      'status' => $body->status,
      'url' => $body->web_url,
    ];
    $build_job->setDetails($details);
    $build_job->save();

    return $response;
  }

  /**
   * @inheritdoc
   */
  protected function updateBuild(BuildJobInterface $build_job): ?string {
    $details = $build_job->getDetails();
    if (!isset($details['pipeline_id'])) {
      return NULL;
    }

    // Get pipeline status.
    $project_id = $this->configuration['gitlab_project_id'];
    $pipeline_id = $details['pipeline_id'];
    $pipeline_response = $this->gitlabApi->getPipeline($project_id, $pipeline_id);
    $pipeline_data = json_decode($pipeline_response->getBody());
    $status = $this->convertStatus($pipeline_data->status);

    // Update job.
    $build_job->setStatus($status);
    $build_job->save();

    // Get job statuses.
    $job_response = $this->gitlabApi->getPipelineJobs($project_id, $pipeline_id);
    $jobs_data = json_decode($job_response->getBody());
    foreach ($jobs_data as $job) {
      $details['jobs'][$job->id] = [
        'id' => $job->id,
        'name' => $job->name,
        'status' => $this->convertStatus($job->status),
        'stage' => $job->stage,
        'created' => $job->created_at ? strtotime($job->created_at) : NULL,
        'started' => $job->started_at ? strtotime($job->started_at) : NULL,
        'finished' => $job->finished_at ? strtotime($job->finished_at) : NULL,
        'url' => $job->web_url,
      ];
    }

    // Update details.
    $build_job->setDetails($details);
    $build_job->save();

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $details): array {

    // Check for an error.
    $error = NULL;
    if (isset($details['message']) && (!isset($details['status']) || $details['status'] == 'error')) {
      $error = $details['message'];
    }

    // Get job details if requested
    $job_log = NULL;
    if ($job_id = $this->requestStack->getCurrentRequest()->query->get('show_job_log')) {
      try {
        $response = $this->gitlabApi->getJobLog($this->configuration['gitlab_project_id'], $job_id);
        $job_log = $response->getBody();
      }
      catch (GuzzleException $e) {}
    }

    // Arrange jobs into stages.
    $gitlab_settings = $this->configFactory->get('build_trigger_gitlab_pipeline.settings');
    $gitlab_stages = explode("\r\n", $gitlab_settings->get('gitlab_stages'));
    $stages = [];
    if (!empty($details['jobs'])) {
      foreach ($gitlab_stages as $stage) {
        foreach ($details['jobs'] as $job) {
          if ($job['stage'] == $stage && $job['status'] != BuildJobInterface::STATUS_SKIPPED) {
            $stages[$stage][$job['id']] = $job;
            if ($job_log && $job['id'] == $job_id) {
              $stages[$stage][$job['id']]['log'] = $job_log;
            }
          }
        }
      }
    }
    foreach ($stages as $key => $stage) {
      ksort($stages[$key]);
    }

    return [
      '#theme' => 'gitlab_pipeline_details',
      '#details' => $details,
      '#error' => $error,
      '#stages' => $stages,
    ];
  }

  /**
   * Convert Gitlab pipline status into BuildJob status.
   *
   * @param string $gitlab_status
   *   Gitlab status code.
   *
   * @return string
   *   Build job status code.
   */
  protected function convertStatus(string $gitlab_status): string {

    switch ($gitlab_status) {
      case 'created':
        return BuildJobInterface::STATUS_CREATED;
      case 'waiting_for_resource':
      case 'preparing':
      case  'pending':
        return BuildJobInterface::STATUS_PENDING;
      case 'running':
        return BuildJobInterface::STATUS_RUNNING;
      case 'success':
        return BuildJobInterface::STATUS_SUCCESS;
      case 'failed':
      case 'canceled':
      case 'skipped':
        return BuildJobInterface::STATUS_FAILED;
      case 'manual':
      case 'scheduled':
        return BuildJobInterface::STATUS_SKIPPED;
      default:
        return BuildJobInterface::STATUS_UNKNOWN;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['gitlab_project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gitlab project ID'),
      '#description' => $this->t('ID or URL-encoded path of the project.'),
      '#default_value' => $this->configuration['gitlab_project_id'] ?? '',
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $form['gitlab_ref'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Git reference'),
      '#description' => $this->t('The branch or tag to run the pipeline on.'),
      '#default_value' => $this->configuration['gitlab_ref'] ?? 'POST',
      '#required' => TRUE,
    ];
    $form['gitlab_vars'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Variables to pass to the pipeline.'),
      '#description' => $this->t('One per line in the form `VARIABLE: value`.'),
      '#default_value' => $this->configuration['gitlab_vars'] ?? 'POST',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'gitlab_project_id' => '',
        'gitlab_ref' => '',
        'gitlab_vars' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $this->configuration['gitlab_project_id'] = $form_state->getValue('gitlab_project_id');
      $this->configuration['gitlab_ref'] = $form_state->getValue('gitlab_ref');
      $this->configuration['gitlab_vars'] = $form_state->getValue('gitlab_vars');
    }
  }

}
