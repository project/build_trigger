<?php

namespace Drupal\build_trigger_gitlab_pipeline;

use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Gitlab API request service.
 */
class GitlabApi implements GitlabApiInterface {

  /**
   * The HTTP Client.
   *
   * @var \Psr\Http\Client\ClientInterface;
   */
  protected ClientInterface $httpClient;

  /**
   * Gitlab API token.
   *
   * @var string
   */
  protected string $token = '';

  /**
   * Gitlab API URL.
   *
   * @var string
   */
  protected string $url = '';

  /**
   * Constructs a GitlabApi object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $http_client;

    $config = $config_factory->get('build_trigger_gitlab_pipeline.settings');
    $this->token = $config->get('gitlab_api_token') ?? '';
    $this->url = $config->get('gitlab_url') ? $config->get('gitlab_url') . '/api/v4' : '';
  }

  /**
   * @inheritdoc
   */
  public function createPipeline(string $project_id, string $ref, array $vars): ResponseInterface {
    $path = '/projects/' . $project_id . '/pipeline';
    $variables = [];
    foreach ($vars as $key =>$value) {
      $variables[] = (object) [
        'key' => $key,
        'value' => $value,
      ];
    }
    $params = [
      'json' =>  [
        'ref' => $ref,
        'variables' => $variables,
      ],
    ];
    return $this->sendRequest($path, $params, 'POST');
  }

  /**
   * @inheritdoc
   */
  public function getPipeline(string $project_id, int $pipeline_id): ResponseInterface {
    $path = '/projects/' . $project_id . '/pipelines/' . $pipeline_id;
    return $this->sendRequest($path, [], 'GET');
  }

  /**
   * @inheritdoc
   */
  public function getPipelineJobs(string $project_id, int $pipeline_id): ResponseInterface {
    $path = '/projects/' . $project_id . '/pipelines/' . $pipeline_id . '/jobs';
    return $this->sendRequest($path, [], 'GET');
  }

  /**
   * @inheritdoc
   */
  public function getJob(string $project_id, int $job_id): ResponseInterface {
    $path = '/projects/' . $project_id . '/jobs/' . $job_id;
    return $this->sendRequest($path, [], 'GET');
  }

  /**
   * @inheritdoc
   */
  public function getJobLog(string $project_id, int $job_id): ResponseInterface {
    $path = '/projects/' . $project_id . '/jobs/' . $job_id . '/trace';
    return $this->sendRequest($path, [], 'GET');
  }

  /**
   * Send request to Gitlab API.
   *
   * @param string $path
   *   Gitlab API path.
   * @param array $params
   *   Guzzle request params.
   * @param string $method
   *   HTTP method to use.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response from Gitlab.
   */
  protected function sendRequest(string $path, array $params = [], string $method = 'POST'): ResponseInterface {

    $params['headers']['PRIVATE-TOKEN'] = $this->token;
    $params['headers']['Content-Type'] = 'application/json';
    return $this->httpClient->request($method, $this->url . $path, $params);
  }

}
