/**
 * Update the build jobs page without having to refresh the page.
 */
(function (Drupal, once) {

  // Time between polling for updates.
  const TIMEOUT = 10000;

  Drupal.behaviors.buildJobs = {
    attach: function (context, settings) {
      once('buildJobs', document.body, context).forEach(() => {
        window.setTimeout(updateBuildJobs, 0, 0);
      });
    }
  };

  /**
   * Update build jobs page.
   */
  function updateBuildJobs(count) {
    console.log('update ' + count);
    window.setTimeout(updateBuildJobs, TIMEOUT, count + 1);
  }
})(Drupal, once);
